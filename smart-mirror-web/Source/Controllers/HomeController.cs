﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Util.Store;
using smart_mirror_web.Source.Models;
using smart_mirror_web.Source.Services;

namespace smart_mirror_web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public async Task<ActionResult> Index()
        {
            var model = new HomeModel();
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(CancellationToken.None);

            if (result.Credential != null)
            {

                CalendarModel cm = GoogleCalendar.GetCalendarInfo(result.Credential);

                System.Diagnostics.Debug.WriteLine("Today");
                foreach (var eventItem in cm.Today.Events)
                {
                    System.Diagnostics.Debug.WriteLine(eventItem.Date.ToString("dd/MM/yyyy"));
                    System.Diagnostics.Debug.WriteLine(eventItem.Public);
                    System.Diagnostics.Debug.WriteLine(eventItem.When);
                    System.Diagnostics.Debug.WriteLine(eventItem.Name);
                    System.Diagnostics.Debug.WriteLine(eventItem.Description);


                }

                System.Diagnostics.Debug.WriteLine("Upcomming");
                foreach (var i in cm.Upcomming.EventsOfDate)
                {
                    foreach (var eventItem in i.Events)
                    {
                        System.Diagnostics.Debug.WriteLine(eventItem.Date.ToString("dd/MM/yyyy"));
                        System.Diagnostics.Debug.WriteLine(eventItem.Public);
                        System.Diagnostics.Debug.WriteLine(eventItem.When);
                        System.Diagnostics.Debug.WriteLine(eventItem.Name);
                        System.Diagnostics.Debug.WriteLine(eventItem.Description);

                    }
                }

                model.Calendar = cm;
            }
            else
            {
                return new RedirectResult(result.RedirectUri);
            }

            return View(model);
        }

        // GET: Home
        public ActionResult Chat()
        {
            return View();
        }


    }
}