﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using smart_mirror_web.Source.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2.Mvc;

namespace smart_mirror_web.Source.Services
{
    public class GoogleCalendar
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/calendar-dotnet-quickstart.json
        private static string[] scopes = { CalendarService.Scope.CalendarReadonly };

        private static string ApplicationName = "smart-mirror";

        private static CalendarService service;

        public static CalendarModel GetCalendarInfo(UserCredential credential)
        {

            // Create Google Calendar API service.
            service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            var model = new CalendarModel();
            var today = new EventOfDate(DateTime.Now, new List<EventItem>());
            var upcomming = new Upcomming(new List<EventOfDate>());
            List<Event> allEvents = new List<Event>();

            // List Today events.
            EventsResource.ListRequest request = service.Events.List("phamducminh1990@gmail.com");
            request.TimeMin = DateTime.Now.Date;
            request.TimeMax = DateTime.Now.AddDays(60).Date;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            Events events = request.Execute();
            if (events.Items.Any())
            {
                foreach (var eventItem in events.Items)
                {
                    if (eventItem.Start.DateTime != null && eventItem.Start.DateTime.Value.Date == DateTime.Now.Date)
                    {
                        var when = string.Empty;
                        if (eventItem.Start.DateTime != null && eventItem.End.DateTime != null)
                        {
                            when = eventItem.Start.DateTime.Value.TimeOfDay.ToString(@"hh\:mm") + " " + eventItem.End.DateTime.Value.TimeOfDay.ToString(@"hh\:mm");
                        }

                        today.Events.Add(new EventItem()
                        {
                            Public = eventItem.Visibility != null && eventItem.Visibility.Equals("Public"),
                            Date = DateTime.Now,
                            When = when,
                            Name = eventItem.Summary,
                            Description = eventItem.Description
                        });
                    }
                    else
                    {
                        if (eventItem.ColorId != "11") continue;

                        if (upcomming.EventsOfDate.FirstOrDefault(x => eventItem.Start.DateTime != null && x.Date == eventItem.Start.DateTime.Value.Date) == null)
                        {
                            if (eventItem.Start.DateTime != null)
                            {
                                EventOfDate eod = new EventOfDate(eventItem.Start.DateTime.Value, new List<EventItem>());
                                eod.Events.Add(
                                    new EventItem()
                                    {
                                        Public = false,
                                        Date = eventItem.Start.DateTime.Value,
                                        When = eventItem.Start.DateTime.ToString() + " " + eventItem.End.DateTime.ToString(),
                                        Name = eventItem.Summary,
                                        Description = eventItem.Description
                                    });
                                upcomming.EventsOfDate.Add(eod);
                            }
                        }
                        else
                        {
                            if (eventItem.Start.DateTime != null)
                            {
                                upcomming.EventsOfDate.FirstOrDefault(x =>
                                        eventItem.Start.DateTime != null &&
                                        x.Date == eventItem.Start.DateTime.Value.Date)
                                    ?.Events.Add(new EventItem()
                                    {
                                        Public = false,
                                        Date = eventItem.Start.DateTime.Value,
                                        When = eventItem.Start.DateTime.ToString() + " " + eventItem.End.DateTime.ToString(),
                                        Name = eventItem.Summary,
                                        Description = eventItem.Description
                                    });
                            }
                        }
                    }
                }
            }

            // List Upcomming events.
            request = service.Events.List("en.vietnamese#holiday@group.v.calendar.google.com");
            request.TimeMin = DateTime.Now.Date;
            request.TimeMax = DateTime.Now.AddDays(60).Date;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            events = request.Execute();

            if (events.Items.Any())
            {
                foreach (var eventItem in events.Items)
                {
                    // Today
                    if (eventItem.Start.Date.Equals(DateTime.Now.Date.ToString("yyyy-MM-dd")))
                    {
                        today.Events.Add(new EventItem()
                        {
                            Public = true,
                            Date = DateTime.Now,
                            When = eventItem.Start.DateTime.ToString() + " " + eventItem.End.DateTime.ToString(),
                            Name = eventItem.Summary,
                            Description = eventItem.Description
                        });
                    }
                    else
                    {
                        // Upcomming
                        if (upcomming.EventsOfDate.FirstOrDefault(x => x.Date == DateTime.Parse(eventItem.Start.Date)) == null)
                        {
                            EventOfDate eod = new EventOfDate(DateTime.Parse(eventItem.Start.Date), new List<EventItem>());
                            eod.Events.Add(
                                new EventItem()
                                {
                                    Public = true,
                                    Date = DateTime.Parse(eventItem.Start.Date),
                                    When = eventItem.Start.Date,
                                    Name = eventItem.Summary,
                                    Description = eventItem.Description
                                });
                            upcomming.EventsOfDate.Add(eod);
                        }
                        else
                        {
                            upcomming.EventsOfDate.FirstOrDefault(x => x.Date == DateTime.Parse(eventItem.Start.Date))?.Events.Add(
                                new EventItem()
                                {
                                    Public = true,
                                    Date = DateTime.Parse(eventItem.Start.Date),
                                    When = eventItem.Start.Date,
                                    Name = eventItem.Summary,
                                    Description = eventItem.Description
                                });
                        }
                    }
                }
            }

            today.Events = today.Events.OrderByDescending(x => x.Public).ThenBy(x => x.Date).ToList();

            foreach (var eod in upcomming.EventsOfDate)
            {
                eod.Events = eod.Events.OrderBy(x => x.Public).ThenBy(x => x.Date).ToList();
            }

            upcomming.EventsOfDate = upcomming.EventsOfDate.OrderBy(x => x.Date).ToList();

            model.Today = today;
            model.Upcomming = upcomming;
            return model;
        }

        public static void Test()
        {
            // Define parameters of request.
            var calendars = service.CalendarList.List().Execute();
            List<Event> allEvents = new List<Event>();

            foreach (var calendar in calendars.Items)
            {
                EventsResource.ListRequest request = service.Events.List(calendar.Id);
                request.TimeMin = DateTime.Now.AddDays(-2);
                request.TimeMax = DateTime.Now.AddDays(30);
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.MaxResults = 20;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                // List events.
                Events events = request.Execute();
                if (events.Items != null && events.Items.Count > 0)
                {
                    allEvents.AddRange(events.Items);
                }
            }

            System.Diagnostics.Debug.WriteLine("Upcoming events:");
            if (allEvents.Any())
            {
                foreach (var eventItem in allEvents)
                {
                    string when = eventItem.Start.DateTime.ToString();
                    if (String.IsNullOrEmpty(when))
                    {
                        when = eventItem.Start.Date;
                    }
                    System.Diagnostics.Debug.WriteLine("{0} ({1}) {2}", eventItem.Summary, when, eventItem.ColorId);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No upcoming events found.");
            }
        }
    }
}