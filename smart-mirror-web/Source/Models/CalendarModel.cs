﻿using System;
using System.Collections.Generic;

namespace smart_mirror_web.Source.Models
{
    public class CalendarModel
    {
        public EventOfDate Today { get; set; }
        public Upcomming Upcomming { get; set; }
    }

    public class EventOfDate
    {
        public EventOfDate(DateTime date, List<EventItem> events)
        {
            this.Date = date;
            this.Events = events;
        }

        public DateTime Date { get; set; }
        public List<EventItem> Events { get; set; }
    }

    public class Upcomming
    {
        public Upcomming(List<EventOfDate> eventsOfDate)
        {
            this.EventsOfDate = eventsOfDate;
        }

        public List<EventOfDate> EventsOfDate { get; set; }
    }

    public class EventItem
    {
        public bool Public { get; set; }
        public DateTime Date { get; set; }
        public String When { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
    }
}