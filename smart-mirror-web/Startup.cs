﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(smart_mirror_web.Startup))]
namespace smart_mirror_web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
